'test pep8'

import os
from integration.python_integration_check_base import run_command, \
    AbstractPythonCheckerTest


# pylint: disable=R0904,C0111
class Pep8Test(AbstractPythonCheckerTest):

    def test_all(self):
        cmd = ["unilint", "-spep8", "-l0"]
        output = run_command(self.stackfolder, cmd)
        self.assertEqual(2, output.count('indentation'), output)
        self.assertEqual(2, output.count('missing whitespace after'), output)
        self.assertEqual(2, output.count('expected 2 blank lines'), output)
        self.assertEquals(3, output.count('pack/bin/foo :'), output)
        self.assertEquals(5, output.count('pack/src/foo.py :'), output)
        self.assertEquals(0, output.count('/pack/bin/foo'), output)
        self.assertEquals(0, output.count('/pack/src/foo'), output)

    def test_all_pack(self):
        cmd = ["unilint", "pack", "-spep8", "-l0"]
        output = run_command(self.stackfolder, cmd)
        self.assertEqual(2, output.count('indentation'), output)
        self.assertEqual(2, output.count('missing whitespace after'), output)
        self.assertEqual(2, output.count('expected 2 blank lines'), output)
        self.assertEquals(3, output.count('bin/foo :'), output)
        self.assertEquals(5, output.count('src/foo.py :'), output)
        self.assertEquals(0, output.count('/bin/foo'), output)
        self.assertEquals(0, output.count('/src/foo'), output)

    def test_all_pack_l10(self):
        cmd = ["unilint", "pack", "-spep8", "-l10"]
        output = run_command(self.stackfolder, cmd)
        self.assertEqual(2, output.count('indentation'), output)
        self.assertEqual(2, output.count('missing whitespace after'), output)
        self.assertEqual(2, output.count('expected 2 blank lines'), output)

    def test_all_bin(self):
        cmd = ["unilint", "pack/bin", "-spep8", "-l0"]
        output = run_command(self.stackfolder, cmd)
        self.assertEqual(1, output.count('indentation'), output)
        self.assertEqual(1, output.count('missing whitespace after'), output)
        self.assertEqual(1, output.count('expected 2 blank lines'), output)
        self.assertEquals(3, output.count('foo :'), output)
        self.assertEquals(0, output.count('/foo'), output)

    def test_src_file(self):
        cmd = ["unilint", os.path.join("src", "foo.py"), "-spep8", "-l0"]
        output = run_command(self.package_path, cmd)
        self.assertEqual(1, output.count('indentation'), output)
        self.assertEqual(1, output.count('missing whitespace after'), output)
        self.assertEqual(1, output.count('expected 2 blank lines'), output)
        self.assertEquals(5, output.count('foo.py :'), output)
        self.assertEquals(0, output.count('/foo.py'), output)

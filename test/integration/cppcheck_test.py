'test cppcheck'

from integration.python_integration_check_base import run_command, \
    AbstractPythonCheckerTest


# pylint: disable=R0904,C0111
class CppcheckTest(AbstractPythonCheckerTest):

    def test_all_pack(self):
        cmd = ["unilint", "pack", "-scppcheck", "-l0"]
        output = run_command(self.stackfolder, cmd)
        self.assertEqual(2, output.count('Uninitialized'), output)

    def test_include(self):
        cmd = ["unilint", "pack/include", "-scppcheck", "-l0"]
        output = run_command(self.stackfolder, cmd)
        self.assertEqual(1, output.count('Uninitialized'), output)

    def test_header(self):
        cmd = ["unilint", "pack/include/foo.hpp", "-scppcheck", "-l0"]
        output = run_command(self.stackfolder, cmd)
        self.assertEqual(1, output.count('Uninitialized'), output)

    def test_src(self):
        cmd = ["unilint", "pack/src", "-scppcheck", "-l0"]
        output = run_command(self.stackfolder, cmd)
        self.assertEqual(1, output.count('Uninitialized'), output)

    def test_src_file(self):
        cmd = ["unilint", "pack/src/foo.cpp", "-scppcheck", "-l0"]
        output = run_command(self.stackfolder, cmd)
        self.assertEqual(1, output.count('Uninitialized'), output)

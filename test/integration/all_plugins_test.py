'test checking the output of all plugins together (duplicate detection)'

from integration.python_integration_check_base import run_command, \
    AbstractPythonCheckerTest


# pylint: disable=R0904,C0111
class AllPluginsTest(AbstractPythonCheckerTest):
    """Tests the most common general case that must never fail"""

    def test_all(self):
        cmd = ["unilint", "-l0", "-o"]
        output = run_command(self.stackfolder, cmd)
        self.assertEquals(55, len(output.splitlines()), output)

    def test_all_dot(self):
        cmd = ["unilint", ".", "-l0", "-o"]
        output = run_command(self.stackfolder, cmd)
        self.assertEquals(55, len(output.splitlines()))

    def test_all_pack(self):
        cmd = ["unilint", "pack", "-l0", "-o"]
        output = run_command(self.stackfolder, cmd)
        self.assertEquals(55, len(output.splitlines()))

    def test_all_script(self):
        cmd = ["unilint", "pack/bin", "-l0", "-o"]
        output = run_command(self.stackfolder, cmd)
        self.assertEquals(22, len(output.splitlines()))

    def test_all_src(self):
        cmd = ["unilint", "pack/bin", "-l0"]
        output = run_command(self.stackfolder, cmd)
        self.assertEquals(24, len(output.splitlines()), output)

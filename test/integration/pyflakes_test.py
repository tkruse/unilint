'test pyflakes plugin'

import os
from integration.python_integration_check_base import run_command, \
    AbstractPythonCheckerTest


# pylint: disable=R0904,C0111
class PyflakesTest(AbstractPythonCheckerTest):

    def test_all(self):
        cmd = ["unilint", "-spyflakes"]
        output = run_command(self.stackfolder, cmd)
        self.assertEquals(2,
                          output.count('assigned to but never used'), output)
        self.assertEquals(2, output.count('pack/bin/foo :'), output)
        self.assertEquals(2, output.count('pack/src/foo.py :'), output)
        self.assertEquals(0, output.count('/pack/bin/foo'), output)
        self.assertEquals(0, output.count('/pack/src/foo'), output)

    def test_bin(self):
        cmd = ["unilint", "foo", "-spyflakes"]
        output = run_command(self.bin, cmd)
        self.assertEquals(1,
                          output.count('assigned to but never used'), output)
        self.assertEquals(2, output.count('foo :'), output)
        self.assertEquals(0, output.count('/foo'), output)

    def test_bin_absolute(self):
        cmd = ["unilint", self.bin, "-spyflakes"]
        output = run_command(self.stackfolder, cmd)
        self.assertEquals(1,
                          output.count('assigned to but never used'), output)
        self.assertEquals(2, output.count('foo :'), output)
        self.assertEquals(0, output.count('/foo'), output)

    def test_src(self):
        cmd = ["unilint", "foo.py", "-spyflakes"]
        output = run_command(self.src, cmd)
        self.assertEquals(1,
                          output.count('assigned to but never used'), output)
        self.assertEquals(0, output.count('/foo.py :'), output)
        self.assertEquals(2, output.count('foo.py :'), output)

    def test_file(self):
        cmd = ["unilint", os.path.join("src", "foo.py"), "-spyflakes"]
        output = run_command(self.package_path, cmd)
        self.assertEquals(1,
                          output.count('assigned to but never used'), output)
        self.assertEquals(0, output.count('/foo.py :'), output)
        self.assertEquals(2, output.count('foo.py :'), output)

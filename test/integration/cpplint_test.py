'test cpplint'

from integration.python_integration_check_base import run_command, \
    AbstractPythonCheckerTest


# pylint: disable=R0904,C0111
class CpplintTest(AbstractPythonCheckerTest):

    def test_all_pack(self):
        cmd = ["unilint", "pack", "-scpplint", "-l0"]
        output = run_command(self.stackfolder, cmd)
        self.assertEqual(
            2, output.count('{ should almost always be at the end'), output)

    def test_include(self):
        cmd = ["unilint", "pack/include", "-scpplint", "-l0"]
        output = run_command(self.stackfolder, cmd)
        self.assertEqual(
            1, output.count('{ should almost always be at the end'), output)

    def test_header(self):
        cmd = ["unilint", "pack/include/foo.hpp", "-scpplint", "-l0", "--raw"]
        output = run_command(self.stackfolder, cmd)
        self.assertEqual(
            1, output.count('{ should almost always be at the end'), output)

    def test_src(self):
        cmd = ["unilint", "pack/src", "-scpplint", "-l0"]
        output = run_command(self.stackfolder, cmd)
        self.assertEqual(
            1, output.count('{ should almost always be at the end'), output)

    def test_src_file(self):
        cmd = ["unilint", "pack/src/foo.cpp", "-scpplint", "-l0"]
        output = run_command(self.stackfolder, cmd)
        self.assertEqual(
            1, output.count('{ should almost always be at the end'), output)

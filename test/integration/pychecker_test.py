'test pychecker'

import os
from integration.python_integration_check_base import run_command, \
    AbstractPythonCheckerTest


# pylint: disable=R0904,C0111
class PycheckerTest(AbstractPythonCheckerTest):

    def test_all(self):
        cmd = ["unilint", "-spychecker"]
        output = run_command(self.stackfolder, cmd)
        self.assertEquals(1, output.count('notexist'), output)
        self.assertEquals(0, output.count('pack/bin/foo :'), output)
        self.assertEquals(3, output.count('pack/src/foo.py :'), output)
        self.assertEquals(0, output.count('/pack/bin/foo'), output)
        self.assertEquals(0, output.count('/pack/src/foo'), output)

    def test_bin(self):
        # pychecker cannot handle scripts
        cmd = ["unilint", "foo", "-spychecker"]
        output = run_command(self.bin, cmd)
        self.assertEquals("", output)

    def test_bin_absolute(self):
        # pychecker cannot handle scripts
        cmd = ["unilint", self.bin, "-spychecker"]
        output = run_command(self.stackfolder, cmd)
        self.assertEquals("", output)

    def test_src(self):
        cmd = ["unilint", "foo.py", "-spychecker"]
        output = run_command(self.src, cmd)
        self.assertEquals(1, output.count('notexist'), output)
        self.assertEquals(3, output.count('foo.py :'), output)
        self.assertEquals(0, output.count('pack/src/foo'), output)

    def test_src_absolute(self):
        cmd = ["unilint", self.src, "-spychecker"]
        output = run_command(self.src, cmd)
        self.assertEquals(1, output.count('notexist'), output)
        self.assertEquals(3, output.count('foo.py :'), output)
        self.assertEquals(0, output.count('pack/src/foo'), output)

    def test_src_file(self):
        cmd = ["unilint", os.path.join('src', "foo.py"), "-spychecker"]
        output = run_command(self.package_path, cmd)
        self.assertEquals(1, output.count('notexist'), output)
        self.assertEquals(0, output.count('/foo.py :'), output)
        self.assertEquals(3, output.count('foo.py :'), output)

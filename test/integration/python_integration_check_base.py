'base class setting up mock file structure'

import os
import unittest
import tempfile
import copy
import shutil
import sys
from integration.io_wrapper import StringIO
from unilint.unilint_main import unilint_main

def run_command(path, cmd):
    'runs unilint command and captures output'
    os.chdir(path)
    sys.stdout = outputs = StringIO()
    unilint_main(cmd)
    sys.stdout = sys.__stdout__
    return outputs.getvalue()


def _add_to_file(path, content):
    """Util function to append to file to get a modification"""
    with open(path, 'ab') as fhand:
        fhand.write(content.encode('UTF-8'))


# pylint: disable=R0904,C0111,C0103
class AbstractPythonCheckerTest(unittest.TestCase):
    """
    creates a folder in self.project containing python code with flaws to find
    """
    @classmethod
    def setUpClass(cls):
        # tests runnable from unilint folder with nosetests
        cls.new_environ = copy.copy(os.environ)
        cls.new_environ["PYTHONPATH"] = os.path.join(os.getcwd(), "src")

        cls.orig_cwd = os.getcwd()
        cls.stackfolder = tempfile.mkdtemp()
        cls.package_path = os.path.join(cls.stackfolder, "pack")
        os.makedirs(cls.package_path)
        cls.create_package_structure()

    @classmethod
    def tearDownClass(cls):
        os.chdir(cls.orig_cwd)
        shutil.rmtree(cls.stackfolder)

    @classmethod
    def create_package_structure(cls):
        cls.src = os.path.join(cls.package_path, "src")
        os.makedirs(cls.src)
        cls.include = os.path.join(cls.package_path, "include")
        os.makedirs(cls.include)
        cls.bin = os.path.join(cls.package_path, "bin")
        os.makedirs(cls.bin)
        cls.test = os.path.join(cls.package_path, "test")
        os.makedirs(cls.test)
        cls.doc = os.path.join(cls.package_path, "doc")
        os.makedirs(cls.doc)

        cls.python_script_file = os.path.join(cls.bin, "foo")
        script = """#! /usr/bin/env python
import os

class foo:
  def bar(self,x=[]):
    x.append(3)
    y = self.notexist
"""
        _add_to_file(cls.python_script_file, script)

        # cls.python_init_file = os.path.join(cls.src, "__init__.py")
        # _add_to_file(cls.python_init_file, '"Nothing here"')

        cls.python_init_file = os.path.join(cls.src, "__init_.py")
        _add_to_file(cls.python_init_file, '"empty"')

        cls.python_src_file = os.path.join(cls.src, "foo.py")
        src = """
import os

class foo:
  def bar(self,x=[]):
    x.append(3)
    y = self.notexist
"""
        src += ('    # this line is far too long '
                'this line is far too long this line is far too long')
        _add_to_file(cls.python_src_file, src)

        cls.cpp_src_file = os.path.join(cls.src, "foo.cpp")
        src = """
#include foo.h

void f()
{
    char *p;
    *p = 0;
}

"""
        _add_to_file(cls.cpp_src_file, src)

        cls.cpp_include_file = os.path.join(cls.include, "foo.hpp")
        src = """
#include bar.h

void f()
{
    char *p;
    *p = 0;
}
"""
        _add_to_file(cls.cpp_include_file, src)

'test command line options'

import sys
import unittest
from integration.io_wrapper import StringIO
from integration.python_integration_check_base import run_command
from unilint.unilint_main import unilint_main


# pylint: disable=R0904,C0111
class CLITest(unittest.TestCase):

    def test_list_plugins(self):
        cmd = ["unilint", "-p"]
        output = run_command('.', cmd)
        for plugin in ['pychecker', 'pyflakes', 'common_source', 'pylint',
                       'pep8', 'python_source', 'cpp_source', 'cppcheck']:
            self.assertTrue(plugin in output)

    def test_version(self):
        cmd = ["unilint", "--version"]
        output = run_command('.', cmd)
        self.assertTrue('unilint' in output)

    def test_help(self):
        cmd = ["unilint", "--help"]
        sys.stdout = outputs = StringIO()
        try:
            unilint_main(cmd)
        except SystemExit:
            pass
        sys.stdout = sys.__stdout__
        output = outputs.getvalue()
        self.assertTrue('usage:' in output, output)

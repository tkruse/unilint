'test unilint cli'

import unittest
from mock import patch

from unilint.unilint_main import register_formatter, register_plugin, \
    FORMATTERS, PLUGINS, resolve_plugins
from unilint.unilint_main import extend_maybe, order_issues, remove_duplicates
from mock import Mock


# pylint: disable=R0904,C0111
class HelperFunctionsTest(unittest.TestCase):

    def test_register_formatter(self):
        def foo_fun():
            pass
        register_formatter('foo', foo_fun)
        self.assertTrue('foo' in FORMATTERS)
        self.assertEqual(foo_fun, FORMATTERS['foo'])

    def test_register_plugin(self):
        with patch('unilint.unilint_plugin.UnilintPlugin') as plugin:
            plugin.get_id.return_value = "bar"
            plugin.get_depends.return_value = []
            register_plugin(plugin)
            self.assertTrue('bar' in PLUGINS)
            self.assertEqual(plugin, PLUGINS['bar'])

    def test_extend_maybe_list(self):
        list1 = [1]
        extend_maybe(list1, None)
        self.assertEqual([1], list1)
        extend_maybe(list1, [])
        self.assertEqual([1], list1)
        extend_maybe(list1, [2])
        self.assertEqual([1, 2], list1)

    def test_extend_maybe_dict(self):
        list1 = {1: [2]}
        extend_maybe(list1, None)
        self.assertEqual({1: [2]}, list1)
        extend_maybe(list1, {})
        self.assertEqual({1: [2]}, list1)
        extend_maybe(list1, {3: [4]})
        self.assertEqual({1: [2], 3: [4]}, list1)
        extend_maybe(list1, {1: [5]})
        self.assertEqual({1: [2, 5], 3: [4]}, list1)

    def test_resolve_plugins(self):
        with patch('unilint.unilint_plugin.UnilintPlugin') as plugin:
            with patch('unilint.unilint_plugin.UnilintPlugin') as plugin2:
                plugin.get_id.return_value = "bar1"
                plugin.get_depends.return_value = []
                register_plugin(plugin)
                plugin2.get_id.return_value = "pop1"
                plugin2.get_depends.return_value = []
                register_plugin(plugin2)
                self.assertEqual(1, len(resolve_plugins('bar1', '')))
                self.assertEqual(2, len(resolve_plugins('bar1,pop1', '')))
                self.assertFalse('bar1' in resolve_plugins(None, 'bar1'))
                self.assertTrue('pop1' in resolve_plugins(None, 'bar1'))

    def test_resolve_plugins_depends(self):
        with patch('unilint.unilint_plugin.UnilintPlugin') as plugin:
            with patch('unilint.unilint_plugin.UnilintPlugin') as plugin2:
                plugin.get_id.return_value = "bar2"
                register_plugin(plugin)
                plugin2.get_id.return_value = "pop2"
                register_plugin(plugin2)
                plugin2.get_depends.return_value = ["bar2"]
                self.assertEqual(2, len(resolve_plugins('pop2', '')))

    def test_order_issues(self):
        with patch('unilint.issue.Issue') as issue1:
            with patch('unilint.issue.Issue') as issue2:
                issue1.path = "aaa"
                issue1.line_number_start = 2
                issue2.path = "aaa"
                issue2.line_number_start = 1
                self.assertEqual([issue2, issue1],
                                 order_issues([issue1, issue2]))
                self.assertEqual([issue2, issue1],
                                 order_issues([issue2, issue1]))
                issue1.path = "aaa"
                issue1.line_number_start = 1
                issue1.line_position = 2
                issue2.path = "aaa"
                issue2.line_number_start = 1
                issue2.line_position = 1
                self.assertEqual([issue2, issue1],
                                 order_issues([issue1, issue2]))
                self.assertEqual([issue2, issue1],
                                 order_issues([issue2, issue1]))

    def test_remove_duplicates(self):
        self.assertEqual([], remove_duplicates([]))
        issue1 = Mock()
        issue2 = Mock()
        issue1.path = '/filename'
        issue2.path = '/filename'
        issue1.severity = 10
        issue2.severity = 10
        issue1.line_number_start = None
        issue2.line_number_start = None
        issue1.line_position = None
        issue2.line_position = None
        issue1.message = 'foo'
        issue2.message = 'bar'
        self.assertEqual([issue1, issue2], remove_duplicates([issue1, issue2]))
        issue1.line_number_start = 42
        issue2.line_number_start = 42
        issue1.message = 'line too long'
        issue2.message = 'unused import foo'
        self.assertEqual([issue1, issue2], remove_duplicates([issue1, issue2]))
        issue1.line_number_start = 42
        issue2.line_number_start = 42
        issue1.line_position = -1
        issue2.line_position = -1
        issue1.message = 'unused import bar'
        issue2.message = 'module bar not used'
        self.assertEqual([issue1], remove_duplicates([issue1, issue2]))
        issue1.line_number_start = 42
        issue2.line_number_start = 42
        issue1.line_position = 12
        issue2.line_position = 28
        issue1.message = 'unused import bar'
        issue2.message = 'module foo not used'
        self.assertEqual([issue1, issue2], remove_duplicates([issue1, issue2]))
        issue1.line_number_start = 32
        issue2.line_number_start = 32
        issue1.line_position = 80
        issue2.line_position = -1
        issue1.message = 'line too long (92 characters)'
        issue2.message = 'Line too long (92/80)'
        self.assertEqual([issue1], remove_duplicates([issue1, issue2]))
        issue1.line_number_start = 32
        issue2.line_number_start = 33
        issue1.line_position = 80
        issue2.line_position = -1
        issue1.message = 'line too long (92 characters)'
        issue2.message = 'Line too long (92/80)'
        self.assertEqual([issue1, issue2], remove_duplicates([issue1, issue2]))

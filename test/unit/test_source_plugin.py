'test source file detection'

import os
import unittest
import tempfile
import shutil

from mock import Mock

from unilint.common_source_plugin import CommonSourcePlugin, splitpath


# pylint: disable=R0904,C0111,C0103
class SplitpathTest(unittest.TestCase):

    def test_splitpath(self):
        self.assertEqual([], splitpath(None))
        self.assertEqual([], splitpath(''))
        self.assertEqual([], splitpath('/'))
        self.assertEqual(['foo'], splitpath('/foo'))
        self.assertEqual(['foo', 'bar'], splitpath('/foo/bar'))


# pylint: disable=R0904,C0111
class CommonSourcePluginTest(unittest.TestCase):

    def setUp(self):
        self.root_dir = tempfile.mkdtemp()
        self.bin_path = os.path.join(self.root_dir, "bin")
        self.test_path = os.path.join(self.root_dir, "test")
        self.doc_path = os.path.join(self.root_dir, "doc")
        self.src_path = os.path.join(self.root_dir, "src")
        os.makedirs(self.test_path)
        os.makedirs(self.bin_path)
        os.makedirs(self.doc_path)
        os.makedirs(self.src_path)

    def tearDown(self):
        shutil.rmtree(self.root_dir)

    def test_categorize_type(self):
        with open(os.path.join(self.bin_path, 'foo'), "w") as fhand:
            fhand.write("#! foo")
        with open(os.path.join(self.test_path, 'foo.py'), "w") as fhand:
            fhand.write("#! foo")
        with open(os.path.join(self.doc_path, 'foo.rst'), "w") as fhand:
            fhand.write("#! foo")
        with open(os.path.join(self.src_path, '.foo'), "w") as fhand:
            fhand.write("#! foo")
        with open(os.path.join(self.src_path, '__init__.py'), "w") as fhand:
            fhand.write("#! foo")
        with open(os.path.join(self.src_path, '__init__.py~'), "w") as fhand:
            fhand.write("#! foo")
        with open(os.path.join(self.src_path,
                               '__init__.py.LOCAL.1234'), "w") as fhand:
            fhand.write("#! foo")
        plug = CommonSourcePlugin(Mock())
        result = plug.categorize_type(Mock(), self.root_dir, [], [])
        self.assertEqual({}, result)

        result = plug.categorize_type(Mock(), self.bin_path, [], ['foo'])
        self.assertEqual({os.path.join(self.bin_path, 'foo'): ['script']},
                         result)
        result = plug.categorize_type(Mock(), self.test_path, [], ['foo.py'])
        self.assertEqual({self.test_path: ['test']}, result)
        result = plug.categorize_type(Mock(), self.doc_path, [], ['foo.rst'])
        self.assertEqual({self.doc_path: ['doc']}, result)
        result = plug.categorize_type(Mock(), self.src_path, [], [])
        self.assertEqual({}, result)
        result = plug.categorize_type(Mock(),
                                      self.src_path,
                                      [],
                                      ['__init__.py'])
        self.assertEqual({}, result)
        result = plug.categorize_type(Mock(),
                                      self.src_path,
                                      [],
                                      ['__init__.py~'])
        self.assertEqual(
            {os.path.join(self.src_path, '__init__.py~'): ['backup']}, result)
        result = plug.categorize_type(Mock(),
                                      self.src_path,
                                      [],
                                      ['__init__.py.LOCAL.12'])
        self.assertEqual(
            {os.path.join(self.src_path, '__init__.py.LOCAL.12'): ['backup']},
            result)

        result = plug.categorize_type(Mock(),
                                      os.path.join(self.test_path, 'foo.py'),
                                      [], None)
        self.assertEqual(
            {os.path.join(self.test_path, 'foo.py'): ['script']}, result)

        result = plug.categorize_type(Mock(),
                                      os.path.join(self.bin_path, 'foo'),
                                      [], None)
        self.assertEqual(
            {os.path.join(self.bin_path, 'foo'): ['script']}, result)

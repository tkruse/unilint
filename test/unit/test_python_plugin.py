'test detection of python files'

import os
import unittest
import tempfile
import shutil

from mock import Mock

from unilint.python_source_plugin import PythonSourcePlugin


# pylint: disable=R0904,C0111,C0103
class PythonSourcePluginTest(unittest.TestCase):

    def setUp(self):
        self.root_dir = tempfile.mkdtemp()
        self.src_path = os.path.join(self.root_dir, "src")
        os.makedirs(self.src_path)
        self.include_path = os.path.join(self.root_dir, "include")
        os.makedirs(self.include_path)

    def tearDown(self):
        shutil.rmtree(self.root_dir)

    def test_categorize_type(self):
        with open(os.path.join(self.src_path, 'foo.py'), "w") as fhand:
            fhand.write("#! foo")
        with open(os.path.join(self.src_path, '__init__.py'), "w") as fhand:
            fhand.write("#! foo")
        plug = PythonSourcePlugin(Mock())

        result = plug.categorize_type(Mock(), self.src_path, [], [])
        self.assertEqual({}, result)
        result = plug.categorize_type(Mock(),
                                      self.src_path,
                                      [], ['foo.py', '__init__.py'])
        self.assertEqual(
            {os.path.join(self.src_path, 'foo.py'): ['python-src'],
             os.path.join(self.src_path, '__init__.py'): ['python-src'],
             self.src_path: ['python-package']},
            result)
        result = plug.categorize_type(Mock(),
                                      os.path.join(self.src_path, 'foo.py'),
                                      [], [])
        self.assertEqual(
            {os.path.join(self.src_path, 'foo.py'): ['python-src']}, result)

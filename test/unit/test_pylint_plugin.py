'test pylint functions'

import unittest

from unilint.pylint_plugin import PylintPlugin


# pylint: disable=R0904,C0111
class PylintUnitTest(unittest.TestCase):
    'tests pylint plugin functions in isolation'

    def test_parse_simple(self):
        output = "foo/bar/dotcode_tf.py:39: [C0111, comp1] Missing docstring"
        plugin = PylintPlugin(None)
        issues = plugin.parse(output)
        self.assertEqual(1, len(issues))
        self.assertEqual('Missing docstring', issues[0].message)

    def test_parse_indicator(self):
        output = """foo/tf.py:47: [C0322, foo] Operator empty
self.listen_duration=1
                    ^"""
        plugin = PylintPlugin(None)
        issues = plugin.parse(output)
        self.assertEqual(1, len(issues))
        self.assertEqual('Operator empty', issues[0].message)
        output = """foo/a.py:47: [C1234, foo]Comma not followed by a space
  def bar(self,x=[]):
              ^^"""
        plugin = PylintPlugin(None)
        issues = plugin.parse(output)
        self.assertEqual(1, len(issues))
        self.assertEqual('Comma not followed by a space', issues[0].message)

    def test_parse_multiline(self):
        output = """foo/pep8_test.py:1: [R0801] Similar lines in 4 files
==integration.pychecker_test:49
==integration.pychecker_test:60
==integration.pyflakes_test:54
==integration.pylint_test:52
        os.chdir(self.src)
        sys.stdout = outputs = StringIO()
        unilint_main(cmd)
        sys.stdout = sys.__stdout__
        output = outputs.getvalue()
"""
        plugin = PylintPlugin(None)
        issues = plugin.parse(output)
        self.assertEqual(1, len(issues))
        expected_message = """Similar lines in 4 files
==integration.pychecker_test:49
==integration.pychecker_test:60
==integration.pyflakes_test:54
==integration.pylint_test:52
        os.chdir(self.src)
        sys.stdout = outputs = StringIO()
        unilint_main(cmd)
        sys.stdout = sys.__stdout__
        output = outputs.getvalue()"""
        self.assertEqual(expected_message, issues[0].message)

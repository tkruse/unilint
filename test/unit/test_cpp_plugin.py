'test c++ source classifier'

import os
import unittest
import tempfile
import shutil

from mock import Mock

from unilint.cpp_source_plugin import CppSourcePlugin


# pylint: disable=R0904,C0111,C0103
class CppSourcePluginTest(unittest.TestCase):

    def setUp(self):
        self.root_dir = tempfile.mkdtemp()
        self.src_path = os.path.join(self.root_dir, "src")
        os.makedirs(self.src_path)
        self.include_path = os.path.join(self.root_dir, "include")
        os.makedirs(self.include_path)

    def tearDown(self):
        shutil.rmtree(self.root_dir)

    def test_categorize_type(self):
        with open(os.path.join(self.src_path, 'foo.h'), "w") as fhand:
            fhand.write("#! foo")
        with open(os.path.join(self.src_path, 'foo.cpp'), "w") as fhand:
            fhand.write("#! foo")
        plug = CppSourcePlugin(Mock())

        result = plug.categorize_type(Mock(), self.src_path, [], [])
        self.assertEqual({}, result)
        result = plug.categorize_type(Mock(),
                                      self.src_path, [], ['foo.c', 'foo.h'])
        self.assertEqual({os.path.join(self.src_path, 'foo.c'): ['cpp-file'],
                          os.path.join(self.src_path, 'foo.h'): ['cpp-file']},
                         result)
        result = plug.categorize_type(Mock(),
                                      os.path.join(self.src_path, 'foo.c'),
                                      [], [])
        self.assertEqual({os.path.join(self.src_path, 'foo.c'): ['cpp-file']},
                         result)

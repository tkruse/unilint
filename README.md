This project currently is not continued, check https://github.com/sk-/git-lint/tree/master/gitlint for an alternative

unilint: unified static code analysis reports
=============================================

Works on Linux, depends on GNU find, grep and xargs.
Requires separate installation of static code analysers, currently supports
* pep8
* pychecker
* pylint
* pyflakes
* cppcheck
* cpplint (from pypi)

Install
-------

You can install/update unilint via pip:

    sudo pip install -U unilint

To get the static code analysers, run

    sudo apt-get install pychecker cppcheck
    sudo pip install -U pep8 pyflakes pylint cpplint

You can also get pep8, pyflakes and pylint using apt-get, but those will often be older versions, so decide for yourself.


Usage
-----

    Usage: unilint [OPTIONS] [PATH]

    Options:
      -h, --help            show this help message and exit
      -v, --verbose         verbose output.
      --version             print version and meta information
      --debug               debug output.
      -l LEVEL, --level=LEVEL
                            A number or one of
                            all(=0),warning(=20),style(=10),error(=30).
      -p, --plugins         List Available plugins
      -s SELECTED_PLUGINS, --select-plugins=SELECTED_PLUGINS
                            Choose plugins by comma separated id, all others
                            deselected
      -d DESELECTED_PLUGINS, --deselect-plugins=DESELECTED_PLUGINS
                            Choose plugins by comma separated id, ignored with
                            --select-plugins
      -t, --tests           Also check unit tests
      -g, --generated       Also check generated Files
      --format=FORMAT       Custom line format using flags $f,$F,$M,$L,$P,$C,$T,$S
      -f FORMATTER, --formatter=FORMATTER
                            One of: full,brief,short
      -o, --ordered         Orders issues by file and line
      -r, --raw             Just prints the output of the checker as is


roslint: unilint for the ROS Robot Operating System
===================================================

The roslint command is a small extension to the unilint command which
also takes ROS packages and stacks as arguments, and will look for
python script files in more folders.

    Usage: roslint [OPTIONS] [PATH or STACK or PACKAGE]


Changelog
=========

0.1.11
------

- fix duplicate detection broken in 1.10, removed regadless of line number

0.1.10
------

- more pylint bugs, also use pylint for whole packages
- fix pep8 relative path showing when not desired
- reactive pyflakes, as pychecker was disabled
- some duplicate detection bugs fixed


0.1.9
-----

- Improved pylint behavior, fixed several minor bugs around it

0.1.8
-----

- travis-ci integration tests
- filter warning about multiple statement on one line
- py3k compatibility

0.1.7
-----

- bugfixes around duplicate detection
- disable pychecker as it is vulnerable to malicious code
- earlier output for ordered issue list of folder
- prettier output on KeyboardInterrupt
- fix failing unit test
- code style improved
- fix roslint cli
- cpplint integration test
- fixed cppcheck/cpplint invocation bug
- fix pylint error when calling with filename

0.1.6
-----

- added support for cpplint (from googlecode)

0.1.5
-----

- Bugs fixed when giving a relative path to filename

0.1.4
-----

- passing test files/folders directly works without -t
- fix relative paths display
- removed duplicate "line too long" and mixed tabs messages
- allowing more than one path/file as argument
- refactoring

0.1.3
-----

- fix ordering by line number

0.1.2
-----

- refactoring for better file traversal
- several minor bugs fixed

0.1.1
-----

- fix sorting line number
- several minor bugs fixed

0.1.0
-----
- first release
